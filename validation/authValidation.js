const { check } = require('express-validator');

exports.authValidation = (method) => {
  switch (method) {
    case 'login': {
      return [
        check('email', 'Please include a valid email').isEmail(),
        check('password', 'Password is required').exists()
      ]
    }
   
  }
}