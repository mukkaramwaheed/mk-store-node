const express = require('express');
const {
  getCategorys,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory
} = require('../controllers/category');

const Category = require('../models/Category');

const router = express.Router({ mergeParams: true });

const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');
const { commonValidation } = require('../validation/commonValidation');
const { validation } = require('../middleware/validation');
router.use(protect);
// router.use(authorize('admin'));

router
  .route('/')
  .get(advancedResults(Category), getCategorys)
  .post([commonValidation('name')], validation, createCategory);

router
  .route('/:id')
  .get(getCategory)
  .patch(updateCategory)
  .delete(deleteCategory);

module.exports = router;
