const {chai,server,userData, expect, constants} = require('../config');
const User = require('../../models/User');
const nock = require('nock');



describe("Account", () => {

	const baseUrl = constants.TEST_URL;
	
	const testData = {...userData.users[0]};

	
	
	// afterEach(function(done){
	// 	nock.cleanAll()
	// 	done();
	// })

	/*
	* Test the /GET account
	*
	*/
	describe("/GET account", () => {

		before(function(done){

			 nock(baseUrl)
			.get('/account')
			.reply(200, { data: { "name": testData.name } })
			 done()
			// console.log(nock.activeMocks())
		})
		it("it should return login person name", (done) => {

			chai.request(baseUrl)
			.get('/account')
			.end((err, res) => {	
				expect(res.body.data.name).to.eql(testData.name)
				done();
			});	
		})
	})

	/*
	* Test the /PUT account
	*
	*/
	describe("/PUT account", () => {

		before(function(done){

			testData.name = 'john';
			 nock(baseUrl)
			
			.put('/account')
			.reply(200, { data: { "name": testData.name } })
			 done()
			// console.log(nock.activeMocks())
		})
		it("it should update the name", (done) => {

			chai.request(baseUrl)
			.put('/account')
			.end((err, res) => {	
				expect(res.body.data.name).to.eql(testData.name)
				done();
			});	
		})
	})
	/*
	* Test the /PATCH account/changePassword
	*
	*/
	describe("/PATCH account/changePassword", () => {

		before(function(done){

			 nock(baseUrl)
			
			.patch('/account/changePassword')
			.reply(400)
			 done()
			// console.log(nock.activeMocks())
		})
		it("it should give validation error", (done) => {

			chai.request(baseUrl)
			.patch('/account/changePassword')
			.end((err, res) => {	
				res.should.have.status(400);
				done();
			});	
		})		
	})

	describe("/PATCH account/changePassword", () => {

		
		before(function(done){
			if (!nock.isActive()) nock.activate()
			 nock(baseUrl)
			.intercept('/account/changePassword','PATCH')
			.reply(200,{ data: {
				"currentPassword":"1234566",
				"newPassword" : "123456"
			} })
			 done()
			// console.log(nock.activeMocks())
		})
		
		it("it should update the password", (done) => {

			chai.request(baseUrl)
			.patch('/account/changePassword')
			.send({
				"currentPassword":"1234566",
				"newPassword" : "123456"
			
			})
			.end((err, res) => {	
				
				res.should.have.status(200);
				done();
			});	
		})		
	})
	
});