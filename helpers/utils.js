const fs = require('fs');

/**
 * 
 * Remove the keys from object if key is exist in arrayData
 * 
 * @param {Object} objectData - objectData
 * @param {Array} arrayData - arrayData
 *
 * @return {Object} 
 */

exports.removeKeyFromObject = (arrayData, objectData) => {

    let objectDetail = { ...objectData };

    for (var key in objectDetail) {

        if (arrayData.indexOf(key) === -1) {

            delete objectData[key];
        }

    }
    return objectData;
}


/**
 * 
 * Delete the image from the directory
 * 
 * @param {string} directory - directory
 * @param {string} imageName - imageName
 *
 * 
 */

exports.deleteImageFromFolder = (directory,imageName) => {

    let fileNameWithPath = `public/${directory}/${imageName}`;
    
    if (fs.existsSync(fileNameWithPath)) {
        fs.unlink(fileNameWithPath, (err) => {
            if (err) throw err;
        });
    }
      
}

/**
 * 
 * Delete the images from the directory
 * 
 * @param {string} directory - directory
 * @param {string} imageArray - imageArray
 *
 * 
 */

exports.deleteMultiImgFromFolder = (directory,imageArray) => {

    imageArray.map(function(imageName,index){
        let fileNameWithPath = `public/${directory}/${imageName}`;
    
        if (fs.existsSync(fileNameWithPath)) {
            fs.unlink(fileNameWithPath, (err) => {
                if (err) throw err;
            });
        }
    })
  
      
}

/**
 * 
 * Remove extra spaces between string
 * 
 * @param {string} name - name
 *
 * @return {string} 
 */

 exports.removeSpaces = (name) => {
    return name.replace(/\s+/g, ' ')
 }

 /**
 * 
 * Merge country code and phone number
 * 
 * @param {string} phoneCode - phoneCode
 * @param {string} phone - phone
 *
 * @return {string} 
 */
 exports.getPhoneNumber = (phoneCode,phone) => {
    return `+${phoneCode}${phone}`
 }