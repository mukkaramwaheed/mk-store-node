module.exports.swaggerDocument = {
    definition: {
      openapi: '3.0.2', // Specification (optional, defaults to swagger: '2.0')
      info: {
        description: `# MK node\n\n---\n\n**INFO**:\n\n  * __Host=__ ${process.env.HOST}${process.env.PORT}/api\n * x-auth-token must set in headers to access the private APIs otherwise, you will get a **401 error** not authorized\n`,
        version: "1.0.0"
      
        
      }
      
    },
    
    servers: [`${process.env.DEVELOP}${process.env.PORT}`],
    // Path to the API docs
    apis: ['./swagger-api/*.yaml']
  }

  module.exports.swaggerOptions = {
    explorer: true
  }
  