const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');
const faker = require('faker');
// Load env vars
dotenv.config({ path: './config/config.env' });

// Load models
const User = require('./models/User');
const { isNumber } = require('lodash');

let totalEntries = process.argv[3];

// check num argument is numeric and greater then 0
var isNum = function (num) { return !/[^.[1-9]]*/.test(num); };

// argv[2] not equal to -d for data insertion
if (process.argv[2] !== '-d') {
    if (typeof totalEntries !== "undefined") {

        if (!isNum(totalEntries)) {
            console.log("Number must be numeric and greater then 0 for the entries".red.inverse);
            process.exit(0);

        }
    }
    else {
        console.log('Arugment required how many entries you want to insert e.g 2'.red.inverse);
        process.exit(0);
    }
}

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});


const importData = async () => {
    try {

        let users = [];
        for (let i = 0; i < totalEntries; i++) {
            let newUser = {
                email: faker.internet.email(),
                name: faker.name.firstName(),
                password: "123456"
            };
            users.push(newUser);

            // visual feedback always feels nice!
            console.log(newUser.email);
        }
        await User.insertMany(users);
        console.log('Data Imported...'.green.inverse);

        process.exit();

    } catch (err) {
        console.error(err);
    }
};

// Delete data
const deleteData = async () => {
    try {

        await User.deleteMany();
        console.log('Data Destroyed...'.red.inverse);
        process.exit();
    } catch (err) {
        console.error(err);
    }
};



if (process.argv[2] === '-i') {
    importData();
} else if (process.argv[2] === '-d') {
    deleteData();
}
