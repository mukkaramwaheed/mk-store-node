const express = require('express');
const { commonValidation } = require('../validation/commonValidation');
const { validation } = require('../middleware/validation');
const {
  register,
  login,
  getMe,
  forgotPassword,
  resetPassword,
  confirmEmail
} = require('../controllers/auth');

const router = express.Router();

const { protect } = require('../middleware/auth');

router.post('/register', [ commonValidation('register')] , validation, register);
router.post('/login', login);
router.get('/me', protect, getMe);
router.post('/forgetPassword', [ commonValidation('email')] , validation, forgotPassword);
router.put('/resetpassword/:resettoken', [ commonValidation('password')],validation, resetPassword);
router.get('/confirmemail', confirmEmail);


module.exports = router;
