const mongoose = require('mongoose');
const slugify = require('slugify');
// const deepPopulate = require('mongoose-deep-populate')(mongoose);
const ItemSchema = new mongoose.Schema({
  name: {
    type: String,
    unique:true
  },
  description:{
      type: String
  },
  slug:{
      type: String
  },
  category:{
    type: mongoose.Schema.ObjectId,
    ref: 'Category',
    require:true
  },
  active:{
    type:Boolean,
    default: 'true'
  },
  avatar:{
    type: Array
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
},
{
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
}

);

ItemSchema.pre('save', function(next) {
this.slug = slugify(this.name, { lower: true });
next();
});

// ItemSchema.plugin(deepPopulate);



module.exports = mongoose.model('item', ItemSchema);
