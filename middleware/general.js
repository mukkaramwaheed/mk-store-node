const jwt = require('jsonwebtoken');
const asyncHandler = require('./async');
const { removeSpaces } = require('../helpers/utils')

/**
 * Remove the extra spaces between words
 * 
 * 
 * @return {Object}
 */
exports.trimBody = asyncHandler(async (req, res, next) => {
    
  let data = req.body  
  for(let k in data){
      req.body[k] = removeSpaces(data[k])
   
  }
  next()

});

// exports.countFile = asyncHandler(async (req, res, next) => {
    
//   if()
  
//   next()

// });

