const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const User = require('../models/User');
const logger = require('../wiston-config');


/**
 * Get all users
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */

exports.getUsers = asyncHandler(async (req, res, next) => {

  logger.info("Fetch all the users detail");
  res.status(200).json(res.advancedResults);
});

/**
 * Get single user based on ID
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */

exports.getUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);

  logger.info("Get users by ID");

  res.status(200).json({
    success: true,
    data: user
  });
});

/**
 * Create user
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */
exports.createUser = asyncHandler(async (req, res, next) => {

  const { name, email, password} = req.body;

  let user = await User.findOne({ email });

  if (user) {

    logger.error('Email already exists');
    return next(new ErrorResponse('Email already exists', 409));
    
  }

  user = new User({
    name,
    email,
    password
  });



  // grab token and send to email
  // const confirmEmailToken = user.generateEmailConfirmToken();

  // // Create reset url
  // const confirmEmailURL = `${req.protocol}://${req.get(
  //   'host',
  // )}/api/v1/auth/confirmemail?token=${confirmEmailToken}`;

  // const message = `You are receiving this email because you need to confirm your email address. Please make a GET request to: \n\n ${confirmEmailURL}`;

  await user.save();
  logger.info('User regiser');

  // const sendResult = await sendEmail({
  //   email: user.email,
  //   subject: 'Email confirmation token',
  //   message,
  // });
  
  // logger.info('Email sent')

  res.status(200).json({success: true, data: {}});

});

/**
 * Update user
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */

exports.updateUser = asyncHandler(async (req, res, next) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  logger.info("Update user by ID");

  res.status(200).json({
    success: true,
    data: user
  });
});

/**
 * Delete users
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */

exports.deleteUser = asyncHandler(async (req, res, next) => {
  await User.findByIdAndDelete(req.params.id);
  logger.info("Delete user by ID");

  res.status(200).json({
    success: true,
    data: {}
  });
});
