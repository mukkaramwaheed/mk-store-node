const jwt = require('jsonwebtoken');
const asyncHandler = require('./async');
const { getPhoneNumber } = require('../helpers/utils')

// Protect routes
exports.phoneGenerate = asyncHandler(async (req, res, next) => {
  

  req.body.phone = getPhoneNumber(req.body.phoneCode,req.body.phone)
  next()

});


