const express = require('express')
const {
  getItems,
  getItem,
  createItem,
  updateItem,
  deleteItem
} = require('../controllers/item')

const Item = require('../models/Item')

const router = express.Router({ mergeParams: true })

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')

const { itemValidation } = require('../validation/itemValidation')
const { validation } = require('../middleware/validation')
const { trimBody } = require('../middleware/general')
const { imageUpload, setFilename} = require('../middleware/imageUpload');
const { constants } = require('../helpers/contants');

router.use(protect)
router.use(trimBody)
router.use(imageUpload(constants.tmpDir,2).array('image',2))

// router.use(authorize('admin'))

router
  .route('/')
  .get(advancedResults(Item,{
      path: 'category',
      select: 'name active'
  }), getItems)
  .post(setFilename,[itemValidation('create')], validation, createItem)

router
  .route('/:id')
  .get(getItem)
  .patch(updateItem)
  // .delete(deleteItem)

router.route('/').delete(deleteItem)

module.exports = router
