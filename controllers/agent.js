const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Agent = require('../models/Agent');
const logger = require('../wiston-config');
var ObjectId = require('mongodb').ObjectID;

/**
* Get all Agent
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.getAgents = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

/**
* Get single Agent based on ID
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.getAgent = asyncHandler(async (req, res, next) => {
  const agent = await Agent.findById(req.params.id);
  logger.info(`Create ${agent.type}`)
  res.status(200).json({
    success: true,
    data: agent
  });
});

/**
* Create Agent
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.createAgent = asyncHandler(async (req, res, next) => {

  let data = req.body  
  data.user = req.user.id

  let phoneExist = await Agent.findOne({ phone: data.phone });

  if(phoneExist){
    logger.error('Phone already exists');
    return next(new ErrorResponse('Phone already exist', 409));
  }

  const agent  = await Agent.create(data);

  res.status(201).json({
    success: true,
    data: agent
  });
});

/**
* Update Agent
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.updateAgent = asyncHandler(async (req, res, next) => {

  let data = req.body

  let phoneExist = await Agent.find({user : ObjectId(req.user.id), 
  phone : data.phone, _id: { $ne: ObjectId(req.params.id) }})

  if(phoneExist.length == 0 || phoneExist == null){
  
    const agent = await Agent.findByIdAndUpdate(req.params.id, data, {
      new: true,
      runValidators: true
    });

    if(agent){
      res.status(200).json({
        success: true,
        data: agent
      });
    }
    else{
      logger.error('Record not found against this ID');
      return next(new ErrorResponse('Record not found against this ID', 409));
    }
    
  }
  else{
      logger.error('Phone already exists');
      return next(new ErrorResponse('Phone already exist', 409));
  } 
});

/**
* Delete Agent
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.deleteAgent = asyncHandler(async (req, res, next) => {
  await Agent.findByIdAndDelete(req.params.id);

  res.status(200).json({
    success: true,
    data: {}
  });
});

