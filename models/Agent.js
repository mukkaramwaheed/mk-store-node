const mongoose = require('mongoose');
const AgentSchema = new mongoose.Schema({
  name: {
    type: String
  },
  company: {
    type: String
  },
  country: {
      type: Number
  },
  city: {
      type: Number
  },
  state: {
      type: Number
  },
  phone: {
      type: String,
      unique: true
  },
  notes: {
      type: String
  },
  agentType: {
    type: String
  },
  active:{
    type:Boolean,
    default: 'true'
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  user:{
    type: mongoose.Schema.ObjectId,
    ref: "User"
  }
});


module.exports = mongoose.model('Agent', AgentSchema);
