const express = require('express');
const {
  getAgent,
  getAgents,
  createAgent,
  updateAgent,
  deleteAgent
} = require('../controllers/agent');

const Agent = require('../models/Agent');

const router = express.Router({ mergeParams: true });

const advancedResults = require('../middleware/advancedResults');
const { protect } = require('../middleware/auth');
const { phoneGenerate } = require('../middleware/agent');
const { trimBody } = require('../middleware/general');
const { agentValidation } = require('../validation/agentValidation');
const { validation } = require('../middleware/validation');

router.use(protect);
router.use(phoneGenerate);
router.use(trimBody)
// router.use(authorize('admin'));

router
  .route('/')
  .get(advancedResults(Agent), getAgents)
  .post([agentValidation('agent')], validation, createAgent);

router
  .route('/:id')
  .get(getAgent)
  .patch([agentValidation('agent')], validation, updateAgent)
  .delete(deleteAgent);

module.exports = router;
