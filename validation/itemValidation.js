const { check } = require('express-validator');



exports.itemValidation = (method) => {
  switch (method) {
  
    case 'create': {
      return [
        check('name', 'Name is required').not().isEmpty().trim().escape(),
        check('description', 'Description is required').trim().escape().optional({nullable: true}),
        // check('price', 'Price must be numeric').isNumeric().trim().escape(),
        // check('quantity','Quantity must be numeric').isNumeric().trim().escape(),
        // check('discount_price','Discount price must be numeric').isNumeric().trim().escape().optional({ checkFalsy: true }),
        // check('language', 'Language required').not().isEmpty().isIn(constants.language).withMessage(message.language).trim().escape(),
        check('category', 'Category must define').isMongoId().trim().escape()
        
      ]
    }
    default: {
      return []
    }

  }
}