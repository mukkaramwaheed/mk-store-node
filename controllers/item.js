const asyncHandler = require('../middleware/async');
const Item = require('../models/Item');

exports.getItems = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc      Get single Item
// @route     GET /api/v1/Item/:id
// @access    Private/Admin
exports.getItem = asyncHandler(async (req, res, next) => {
  const item = await Item.findById(req.params.id);

  res.status(200).json({
    success: true,
    data: item
  });
});

// @desc      Create Item
// @route     POST /api/v1/Item
// @access    Private/Admin
exports.createItem = asyncHandler(async (req, res, next) => {


  const item = await Item.create(req.body);

  res.status(201).json({
    success: true,
    data: item
  });
});

// @desc      Update Item
// @route     PUT /api/v1/Item/:id
// @access    Private/Admin
exports.updateItem = asyncHandler(async (req, res, next) => {
  const item = await Item.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({
    success: true,
    data: item
  });
});

// @desc      Delete Item
// @route     DELETE /api/v1/Item/:id
// @access    Private/Admin
exports.deleteItem = asyncHandler(async (req, res, next) => {

  // await Item.findByIdAndDelete(req.params.id);
  await Item.deleteMany()
  res.status(200).json({
    success: true,
    data: {}
  });
});