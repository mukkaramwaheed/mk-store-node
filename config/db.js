const mongoose = require('mongoose');

const connectDB = async () => {

  let dbUrl = '';
  if (process.env.NODE_ENV == 'production') {

    dbUrl = 'mongodb+srv://mukkaramwaheed:qwertyu1@cluster0.zd72t.mongodb.net/devcamper?retryWrites=true&w=majority';
  }
  else {

    dbUrl = process.env.MONGO_URI;
    // dbUrl = 'mongodb+srv://mukkaramwaheed:qwertyu1@cluster0.zd72t.mongodb.net/devcamper?retryWrites=true&w=majority';

  }

  const conn = await mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  });

  console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.underline.bold);
};

module.exports = connectDB;
