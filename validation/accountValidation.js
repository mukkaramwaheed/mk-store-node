const { check } = require('express-validator');
const { constants } = require('../helpers/contants');

exports.accountValidation = (method) => {
  switch (method) {
    case 'profile': {
        return [
          check('name', 'Name is required').not().isEmpty().trim().escape(),
          check('phone', 'Phone must be numeric').isNumeric().optional({ checkFalsy: true }).trim().escape(),
          check('language', 'Language required').not().isEmpty().isIn(['english', 'dutch']).withMessage('Language must be english or dutch').trim().escape(),
        ]
      }
      case 'changePassword': {
        return [
            check(
                'currentPassword',
                `Please enter a current password with ${constants.passwordLength} or more characters`
              ).isLength({ min: constants.passwordLength }).trim().escape(),
              check(
                'newPassword',
                `Please enter a new password with ${constants.passwordLength} or more characters`
              ).isLength({ min: constants.passwordLength }).trim().escape(),
            
        ]
      }
  }
}