const multer = require("multer");
const { constants } = require('../helpers/contants');
const crypto = require("crypto");
const filesize = require("filesize"); 


/**
 * 
 * Upload image configuration
 * 
 */
exports.imageUpload = (directory,maxlimit=1) => {

    
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, `public/${directory}`)
        },
        filename: async (req, file, callback) => {
        req.limit = maxlimit

        // var fileSizeInMb = filesize(file.originalname, {round: 0})
        // console.log(fileSizeInMb)
        // if(req.files.length <= maxlimit){
        var filetype = '';
        if (file.mimetype === 'image/jpg') {
            filetype = 'gif';
        }
        if (file.mimetype === 'image/png') {
            filetype = 'png';
        }
        if (file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        callback(null, 'image-' + crypto.randomBytes(3).toString('hex') + '-' + Date.now() + '.' + filetype);

        // }
        // else{
        //     return callback(new Error('File limit is' + maxlimit));
        // }
            
        }

    })
    var upload = multer({
        storage: storage, fileFilter: (req, file, callback) => {
          
            if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
                
                callback(null, true);
            } else {
                callback(null, false);
                return callback(new Error('Only .png, .jpg and .jpeg format allowed!'));
            }
        },
        limits: { fileSize: 1 * 1024 * 1024 }
    });
    return upload;
};

/** 
 * 
 * Set filename in req after image upload successfully
*/

exports.setFilename = (req,res,next) => {
    
    if(req.body.request){
        req.body = JSON.parse(req.body.request) 
    }

    if (Array.isArray(req.files)) {
        let avatar = []

        req.files.forEach(function(val,index){
            avatar.push(val.filename)
        })
        req.body.avatar = avatar
        
        // req.body.avatar.push(req.files)
    } 
    else if (req.file) {
        
        req.body.avatar = req.file.filename;
    }
    
    return next()
       
}