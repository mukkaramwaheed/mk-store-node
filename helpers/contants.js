exports.constants = {
    privileges: ['user'],
    language:['english','dutch'],
    passwordLength: 6,
    getProfile: [
        'name',
        '_id',
        'language',
        'phone',
        'active',
        'avatar'
    ],
    imageDir: 'images',
    tmpDir: 'temp',
    agentType:['supplier','customer']
}