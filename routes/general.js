const express = require('express');
const {
    getAllCountries,
    getStatesOfCountry,
    getCitiesOfState
} = require('../controllers/general');

const router = express.Router({ mergeParams: true });


router.get('/getAllCountries',getAllCountries);
router.get('/getStatesOfCountry/:id',getStatesOfCountry);
router.get('/getCitiesOfState/:id',getCitiesOfState);



module.exports = router;
