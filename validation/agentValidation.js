const { check } = require('express-validator');
const { constants } = require('../helpers/contants');

exports.agentValidation = (method) => {
  switch (method) {
    case 'agent': {
      return [
        check('name', 'Name required').not().isEmpty().trim().escape()
        .matches(/^[A-Za-z\s]+$/).withMessage('Name must be alphabetic.'),
        check('phone', 'Phone required').not().isEmpty()
        .isNumeric().withMessage('Phone must be numeric and without spaces').trim().escape(),
        check('phoneCode', 'Phone code required').isNumeric().trim().escape(),
        check('country','Country required').not().isEmpty().trim().escape(),
        check('agentType','Type required')
        .not().isEmpty().isIn(constants.agentType).withMessage("Invalid agent type").trim().escape()
        
      ]
    } 
  }
}
