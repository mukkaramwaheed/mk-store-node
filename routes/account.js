const express = require('express');
const { commonValidation } = require('../validation/commonValidation');
const { accountValidation } = require('../validation/accountValidation');
const { validation } = require('../middleware/validation');
const { imageUpload, setFilename} = require('../middleware/imageUpload');
const {
  getProfile,
  updateProfile,
  changePassword,
  deleteImage
} = require('../controllers/account');



const router = express.Router({ mergeParams: true });
const { protect } = require('../middleware/auth');
router.use(protect);

router.use(imageUpload().single('image'));

router
  .get('/', getProfile)
  .patch('/', setFilename, [commonValidation('updateProfile')], validation, updateProfile)
  .patch('/changePassword', [accountValidation('changePassword')], validation, changePassword)
  .delete('/image', deleteImage)

  // .delete(deleteUser);
  // .post('/test', upload.single('avatar'), async (req, res, next) => {
  //   try {
  
  //     res.send({ fileUrl: 'http://localhost:5000/images/' + req.file.filename })

  //   } catch (error) {
  //     res.send(error);
  //   }
  // })

module.exports = router;
