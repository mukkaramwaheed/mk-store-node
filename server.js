const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
// const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');
const errorHandler = require('./middleware/error');
const connectDB = require('./config/db');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const bodyParser = require('body-parser');
const ErrorResponse = require('./utils/errorResponse');

// Load env vars
dotenv.config({ path: './config/config.env' });

const { swaggerDocument, swaggerOptions } = require('./swagger.config.js')

const logger = require('./wiston-config');

// Connect to database
connectDB();

// Route files
const auth = require('./routes/auth');
const users = require('./routes/users');
const account = require('./routes/account');
const category = require('./routes/category');
const item = require('./routes/item');
const agent = require('./routes/agent');
const general = require('./routes/general');

const app = express();

// Body parser
app.use(express.json());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true})) 

// Cookie parser
app.use(cookieParser());

// Dev logging middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
  app.use(
    morgan('dev', {
      stream: logger.stream
      // only log error responses
      // skip: (req, res) => {
      //     return res.statusCode < 400;
      // },
    })
  )
}

// File uploading
// app.use(fileupload());

// Sanitize data
app.use(mongoSanitize());

// Set security headers
app.use(helmet());

// Prevent XSS attacks
app.use(xss());

// Rate limiting
const limiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 10 mins
  max: 100
});
app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Enable CORS
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

const swaggerDocs = swaggerJSDoc(swaggerDocument)
app.use(
  '/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs, swaggerOptions)
)



// Mount routers
app.use('/api/v1/auth', auth);
app.use('/api/v1/user', users);
app.use('/api/v1/account', account);
app.use('/api/v1/category',category);
app.use('/api/v1/item',item);
app.use('/api/v1/agent',agent);
app.use('/api/v1/',general);

// throw 404 if URL not found
app.all("*", function(req, res,next) {
	   logger.error('Resource not found');
      return next(new ErrorResponse('Resource not found', 404));
});

app.use(errorHandler);

const PORT = process.env.PORT || 5000;


const server = app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red);
  logger.error(`Error: ${err.message}`)
  // Close server & exit process
  server.close(() => process.exit(1));
});

module.exports = server;  