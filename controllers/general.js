const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const logger = require('../wiston-config');
let csc = require('country-state-city').default
/**
* Get all countries
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.getAllCountries = asyncHandler(async (req, res, next) => {

    res.status(200).json({
      success: true,
      data: csc.getAllCountries()
    });
  });

  /**
* Get state of country using country Id
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.getStatesOfCountry = asyncHandler(async (req, res, next) => {

  const allStates = csc.getStatesOfCountry(req.params.id);
 
  if(allStates == ''){
       logger.error('No state found');
       return next(new ErrorResponse('No state found', 404));
  }

  res.status(200).json({
    success: true,
    data: allStates
  });
});
  
/**
* Get cities of state using state Id
*
* @param {Object} req - request object
* @param {Object} res - response object
* @param {Function} next - go to the next middleware
*
* @return {Object}
*/
exports.getCitiesOfState = asyncHandler(async (req, res, next) => {

  const allCities = csc.getCitiesOfState(req.params.id);
 
  if(allCities == ''){
       logger.error('No cities found');
       return next(new ErrorResponse('No cities found', 404));
  }

  res.status(200).json({
    success: true,
    data: allCities
  });
});