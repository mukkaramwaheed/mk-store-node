const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { removeKeyFromObject } = require('../helpers/utils');
const { constants } = require('../helpers/contants');
const crypto = require('crypto');


const UserSchema = new mongoose.Schema({
  name: {
  type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    index: true,
    lowercase:true
  },
  password: {
    type: String,
    required: true,
    select:false
  },
  avatar: {
    type: String,
  },
  phone:{
    type:String
  },
  active:{
    type:Boolean,
    default: 'true'
  },
  privileges:{
    type:Object
  },
  language:{
    type: String,
    enum: ['english'],
    default: 'english'
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  lastLogin:{
    type: Date
  },
  role:{
    type:String,
    enum:['user','admin'],
    default:'user'
  },
  resetPasswordToken: {
    type:String,
    select:false
  },
  resetTokenLink:{
    type:String,
    select:false
  },
  resetPasswordExpire: {
    type:Date,
    select:false
  },
  confirmEmailToken:{
    type:String,
    select:false
  },
  isEmailConfirmed: {
    type: Boolean,
    default: false,
    select:false
  },
  twoFactorCode: {
    type:String,
    select:false
  },
  twoFactorCodeExpire: {type:Date,
    select:false
  },
  twoFactorEnable: {
    type: Boolean,
    default: false,
    select:false
  }
});

// Encrypt password using bcrypt
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }

  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
  
});


// Sign JWT and return
UserSchema.methods.getSignedJwtToken = function () {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};

// Match user entered password to hashed password in database
UserSchema.methods.matchPassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};


// Retrieves a users profile without exposing any sensitive information.
 
UserSchema.methods.getProfile = function(){
  var user = this.toObject();


  let userDetai = removeKeyFromObject(constants.getProfile,user);
  return userDetai;

}

// Generate and hash password token
UserSchema.methods.getResetPasswordToken = function () {
  // Generate token
  const resetToken = crypto.randomBytes(20).toString('hex');

  //save the reset token in collection for testing 
  this.resetTokenLink = resetToken;

  // Hash token and set to resetPasswordToken field
  this.resetPasswordToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');

  // Set expire
  this.resetPasswordExpire = Date.now() + 30 * 60 * 1000;

  return resetToken;
};

// Generate email confirm token
UserSchema.methods.generateEmailConfirmToken = function (next) {
  // email confirmation token
  const confirmationToken = crypto.randomBytes(20).toString('hex');

  this.confirmEmailToken = crypto
    .createHash('sha256')
    .update(confirmationToken)
    .digest('hex');

  const confirmTokenExtend = crypto.randomBytes(100).toString('hex');
  const confirmTokenCombined = `${confirmationToken}.${confirmTokenExtend}`;
  return confirmTokenCombined;
};

module.exports = mongoose.model('User', UserSchema);
