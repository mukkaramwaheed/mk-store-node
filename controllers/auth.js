const crypto = require('crypto');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const User = require('../models/User');
const sendEmail = require('../utils/sendEmail');
const logger = require('../wiston-config');
// var	debug = require('debug')('bb-control-api:controllers/auth');




/**
 * Register a user
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */

exports.register = asyncHandler(async (req, res, next) => {
    
  const { name, email, password} = req.body;

  let user = await User.findOne({ email });

  if (user) {

    logger.error('Email already exists');
    return next(new ErrorResponse('Email already exists', 409));
    
  }

  user = new User({
    name,
    email,
    password
  });



  // grab token and send to email
  const confirmEmailToken = user.generateEmailConfirmToken();

  // Create reset url
  const confirmEmailURL = `${req.protocol}://${req.get(
    'host',
  )}/api/v1/auth/confirmemail?token=${confirmEmailToken}`;

  const message = `You are receiving this email because you need to confirm your email address. Please make a GET request to: \n\n ${confirmEmailURL}`;

  await user.save();
  logger.info('User regiser');

  // const sendResult = await sendEmail({
  //   email: user.email,
  //   subject: 'Email confirmation token',
  //   message,
  // });
  
  logger.info('Email sent')

  const token = user.getSignedJwtToken();
  logger.info('Token created');
  res.status(200).json({
        success: true,
        token,
        confirmEmailToken
      });
});


/**
 * Authenticate a user based on email and password.
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object} 
 */

exports.login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  // Validate emil & password
  if (!email || !password) {

    logger.error('Email and password not set');
    return next(new ErrorResponse('Please provide an email and password', 400));
  }

  // Check for user
  const user = await User.findOne({ email }).select('+password');
  

  if (!user) {

    logger.error('There is no user with that email');
    return next(new ErrorResponse('There is no user with that email', 401));
  }

  // Check if password matches
  const isMatch = await user.matchPassword(password);
  
  if (!isMatch) {

    logger.error('Invalid credentials');
    return next(new ErrorResponse('Invalid credentials', 401));
  }

  //if active is false then user not allow to login
  if(!user.active){

    logger.error('Account is not active contact to support');
    return next(new ErrorResponse('Account is not active contact to support', 401));
  }

  const token = user.getSignedJwtToken();
  
  logger.info('Token created');
  res.status(200).json({
        success: true,
        token,
      });
});



/**
 * Get the current user based on the access token.
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object}
 */

exports.getMe = asyncHandler(async (req, res, next) => {

  // user is already available in req due to the protect middleware
  const user = req.user;
  
  logger.info('User detail get');
  res.status(200).json({
    success: true,
    data: user,
  });
});

/**
 * Check email exist then send reset password link.
 * 
 * @param {Object} req - request object
 * @param {Object} res - response object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object}
 */
exports.forgotPassword = asyncHandler(async (req, res, next) => {
  
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    logger.error('There is no user with that email');
    return next(new ErrorResponse('There is no user with that email', 404));
  }

  // Get reset token
  const resetToken = user.getResetPasswordToken();

  await user.save({ validateBeforeSave: false });

  // Create reset url
  const resetUrl = `${req.protocol}://${req.get(
    'host',
  )}/api/v1/auth/resetpassword/${resetToken}`;

  const message = `You are receiving this email because you (or someone else) has requested the reset of a password. Please make a PUT request to: \n\n ${resetUrl}`;

  try {
    // await sendEmail({
    //   email: user.email,
    //   subject: 'Password reset token',
    //   message,
    // });

    logger.info('Email sent');
    res.status(200).json({ success: true, data: 'Email sent' });
  } catch (err) {
    console.log(err);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    await user.save({ validateBeforeSave: false });

    logger.error('Email could not be sent');
    return next(new ErrorResponse('Email could not be sent', 500));
  }
});


/**
 * Reset the password
 * 
 * @param {Object} req - request object
 * @param {Object} res - response object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object}
 */
exports.resetPassword = asyncHandler(async (req, res, next) => {
  // Get hashed token
  const resetPasswordToken = crypto
    .createHash('sha256')
    .update(req.params.resettoken)
    .digest('hex');

  const user = await User.findOne({
    resetPasswordToken,
    resetPasswordExpire: { $gt: Date.now() },
  });

  if (!user) {

    logger.error('Invalid token');
    return next(new ErrorResponse('Invalid token', 400));
  }

  // Set new password
  user.password = req.body.password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpire = undefined;
  await user.save();

  const token = user.getSignedJwtToken();

  logger.info('Token created');
  res.status(200).json({
        success: true,
        token,
      });
});

/**
 * Confirm email 
 * 
 * @param {Object} req - request object
 * @param {Object} res - response object
 * @param {Function} next - go to the next middleware
 *
 * @return {Object}
 */
exports.confirmEmail = asyncHandler(async (req, res, next) => {
  // grab token from email
  const { token } = req.query;

  if (!token) {
    logger.error('Invalid token');
    return next(new ErrorResponse('Invalid Token', 400));
  }

  const splitToken = token.split('.')[0];
  const confirmEmailToken = crypto
    .createHash('sha256')
    .update(splitToken)
    .digest('hex');

  // get user by token
  const user = await User.findOne({
    confirmEmailToken,
    isEmailConfirmed: false,
  });

  if (!user) {
    logger.error('Invalid token');
    return next(new ErrorResponse('Invalid token', 400));
  }

  // update confirmed to true
  user.confirmEmailToken = undefined;
  user.isEmailConfirmed = true;

  // save
  await user.save();
  logger.info('User detail update');
  logger.info('Token created');

  // return token
  res.status(200).json({
        success: true,
        token : user.getSignedJwtToken(),
      });
});

