const { check } = require('express-validator');
const { constants } = require('../helpers/contants');


exports.commonValidation = (method) => {
  switch (method) {
  
    case 'updateProfile': {
      return [
        check('name', 'Name is required').not().isEmpty().trim().escape(),
        check('phone', 'Phone must be numeric').isNumeric().optional({ checkFalsy: true }).trim().escape(),
        // check('language', 'Language required').not().isEmpty().isIn(constants.language).withMessage(message.language).trim().escape(),
        check('active', 'Value must be true or false').optional().isBoolean().trim().escape(),
        
      ]
    }
    case 'register': {

      return [
        check('name', 'Name is required').not().isEmpty().trim().escape(),
        check('phone', 'Phone must be numeric').isNumeric().optional({ checkFalsy: true }).trim().escape(),
        // check('language', 'Language required').not().isEmpty().isIn(constants.language).withMessage(message.language).trim().escape(),
        check('active', 'Value must be true or false').optional().isBoolean().trim().escape(),

        check('email', 'Please include a valid email').isEmail().trim().escape(),
        check(
          'password',
          `Please enter a password with ${constants.passwordLength} or more characters`
        ).isLength({ min: constants.passwordLength }).trim().escape()

        // check('holding').optional({ checkFalsy: true }).trim().escape(),
        // check('project').optional({ checkFalsy: true }).trim().escape(),
        // check("privileges")
        //   .custom(async (value, { req }) => {
        //     var keys = Object.keys(value);
        //     for (var k = 0; k < keys.length; k++) {
        //       var key = keys[k];
        //       if (!privilegesArray.includes(key)) throw new Error("Topic key is invalid.");
        //       if (
        //         value[key] !== true &&
        //         value[key] !== false &&
        //         value[key] !== ""
        //       ) {
        //         throw new Error("Topic value is invalid.");
        //       }
        //     }
        //     return true;
        //   })
        //   .withMessage("Invalid privileges")
        //   .optional({ checkFalsy: true })
      ]
    }
    case 'name': {
      return [
        check('name', 'Name is required').not().isEmpty().trim().escape()
        .matches(/^[A-Za-z\s]+$/).withMessage('Name must be alphabetic.')
      
      ]
    }
    case 'phone': {
      return [
        check('phone', 'Phone is required').not().isEmpty().trim().escape()
        .matches(/^[0-9\s]+$/).withMessage('Phone must be numeric.')
      
      ]
    }
    case 'email':{
      return [
        check('email', 'Please include a valid email').isEmail(),

      ]
    }
    case 'password': {
      return [
        check(
          'password',
          `Please enter a password with ${constants.passwordLength} or more characters`
        ).isLength({ min: constants.passwordLength }).trim().escape()
      ]
    }
  }
}