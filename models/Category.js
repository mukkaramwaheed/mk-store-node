const mongoose = require('mongoose');
const CategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique:true
  },
  active:{
    type:Boolean,
    default: 'true'
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});



module.exports = mongoose.model('Category', CategorySchema);
