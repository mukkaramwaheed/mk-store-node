const {chai,server,userData, expect} = require('../config');
const User = require('../../models/User');


describe("Auth", () => {



	const baseUrl = '/api/v1/auth/';
	// Before each test we empty the database
	before((done) => {
		User.deleteMany({}, (err) => {
			done();
		});
	});

	// Prepare data for testing
	const testData = userData.users[0];

	/*
  * Test the /POST route
  */
	describe("/POST Register", () => {


		it("It should send validation error for Register", (done) => {
			chai.request(server)
				.post(`${baseUrl}register`)
				.set('Accept', 'application/json')
				.send({ "email": testData.email })
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
		});


	});

	/*
 * Test the /POST route
 */
	describe("/POST Register", () => {
		it("It should Register user", (done) => {
			chai.request(server)
				.post(`${baseUrl}register`)
				.set('Accept', 'application/json')
				.send(testData)
				.end((err, res) => {
					res.body.should.have.property("token")
					done();
				});
		});

	});

	describe("/POST Register", () => {
		it("It should return error email already exist", (done) => {
			chai.request(server)
				.post(`${baseUrl}register`)
				.set('Accept', 'application/json')
				.send(testData)
				.end((err, res) => {

					res.should.have.status(500);
					res.body.should.have.property("error").eql("Email already exists");

					done();
				});
		});
	});

	/*
	   * Test the /POST route
	  */
	// 	describe("/POST Login", () => {
	// 		it("it should Send account not confirm notice.", (done) => {
	// 			chai.request(server)
	// 				.post("/api/auth/login")
	// 				.send({"email": testData.email,"password": testData.password})
	// 				.end((err, res) => {
	// 					res.should.have.status(401);
	// 					res.body.should.have.property("message").eql("Account is not confirmed. Please confirm your account.");
	// 					done();
	// 				});
	// 		});
	// 	});

	// 	/*
	//   * Test the /POST route
	//   */
	// 	describe("/POST Resend  Confirm OTP", () => {
	// 		it("It should resend  confirm OTP", (done) => {
	// 			chai.request(server)
	// 				.post("/api/auth/resend-verify-otp")
	// 				.send({"email": testData.email})
	// 				.end((err, res) => {
	// 					res.should.have.status(200);
	// 					UserModel.findOne({_id: testData._id},"confirmOTP").then((user)=>{                
	// 						testData.confirmOTP = user.confirmOTP;
	// 						done();
	// 					});
	// 				});
	// 		});
	// 	});

	// 	/*
	//   * Test the /POST route
	//   */
	// 	describe("/POST Verify Confirm OTP", () => {
	// 		it("It should verify confirm OTP", (done) => {
	// 			chai.request(server)
	// 				.post("/api/auth/verify-otp")
	// 				.send({"email": testData.email, "otp": testData.confirmOTP})
	// 				.end((err, res) => {
	// 					res.should.have.status(200);
	// 					done();
	// 				});
	// 		});
	// 	});

	/*
  * Test the /POST route
  */
	describe("/POST Login", () => {
		it("It should send validation error for Login", (done) => {
			chai.request(server)
				.post(`${baseUrl}login`)
				.set('Accept', 'application/json')
				.send({ "email": testData.email })
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
		});
	});

	/*
	* Test the /POST route
	*/
	describe("/POST Login", () => {
		it("it should Send failed user Login", (done) => {
			chai.request(server)
				.post(`${baseUrl}login`)
				.send({ "email": "admin@admin.com", "password": "1234" })
				.set('Accept', 'application/json')
				.end((err, res) => {
					res.should.have.status(401);
					done();
				});
		});
	});

	/*
	* Test the /POST route
	*/
	describe("/POST Login", () => {
		it("it should do user Login", (done) => {
			chai.request(server)
				.post(`${baseUrl}login`)
				.set('Accept', 'application/json')
				.send({ "email": testData.email, "password": testData.password })
				.end((err, res) => {
					res.should.have.status(200);
					testData.token = res.body.token
					res.body.should.have.property("token");
					done();
				});
		});
	});

	/*
	* Test the /GET me
	*
	*/
	describe("/GET me", () => {
		it("it should give error token is invalid", (done) => {
			chai.request(server)
			.get(`${baseUrl}me`)
			.set('x-auth-token', testData.token+11)
			.end((err, res) => {
				res.should.have.status(401);
				res.body.should.have.property("error").eql("Not authorized to access this route");
				done();
			});	
		})
	})

	/*
	* Test the /GET me
	*
	*/
	describe("/GET me", () => {
		it("it should return the user detail", (done) => {
			chai.request(server)
			.get(`${baseUrl}me`)
			.set('x-auth-token', testData.token)
			.end((err, res) => {
				res.should.have.status(200);
				res.body.data.should.have.property('email');
				expect(res.body.data.email).to.equal(testData.email);

				// expect(res.body.data).to.have.deep.property('{email}', testData.email);
				// expect(res.body.data).to.be.an('object').that.contains.something.like({'email':testData.email})
				done();
			});	
		})
	})

	/*
	* Test the /forgetPassword
	*
	*/
	describe("/POST forget", () => {
		it("it should return validation error, email not found", (done) => {
			chai.request(server)
			.post(`${baseUrl}forgetPassword`)
			.set('Accept', 'application/json')
			.end((err, res) => {
				res.should.have.status(400);

				// expect(res.body.data).to.have.deep.property('{email}', testData.email);
				expect(res.body.error[0].email).to.equal("Please include a valid email")
				done();
			});	
		})
	})

	/*
	* Test the /forgetPassword
	*
	*/
	describe("/POST forget", () => {
		it("it should display message email sent",  (done) => {
			chai.request(server)
			.post(`${baseUrl}forgetPassword`)
			.set('Accept', 'application/json')
			.send({ "email": testData.email,"password":testData.password})
			.end((err, res) => {
				
				res.should.have.status(200);
				// res.body.data.should.have.property('data').eql('Email sent');
				expect(res.body.data).to.equal("Email sent")

				//set the resetToken in testData 
				User.findOne({email: testData.email},"resetTokenLink").then((user) => {
					testData.resetTokenLink = user.resetTokenLink
					done();
				});
				
				console.log("reset token 2" + testData.resetTokenLink);
			});	
		})
	})

	/*
	* Test the /resetPassword
	*
	*/
	describe("/PUT resetPassword", () => {
		it("it should return invalid token", (done) => {
			chai.request(server)
			.put(`${baseUrl}resetpassword/${testData.resetTokenLink}11`)
			.set('Accept', 'application/json')
			.send({"password":testData.password})
			.end((err, res) => {
				res.should.have.status(400);
				console.log('reset token',testData.resetTokenLink)
				expect(res.body.error).to.equal("Invalid token")
				done();
			});	
		})
	})

	/*
	* Test the /resetpassword
	*
	*/
	describe("/PUT resetPassword", () => {
		it("it should return token",(done) => {
			chai.request(server)
			.put(`${baseUrl}resetpassword/${testData.resetTokenLink}`)
			.set('Accept', 'application/json')
			.send({"password":testData.password})
			.end((err, res) => {
				// console.log(res.body);
				res.should.have.status(200);
				res.body.should.have.property("token");
				done();
			});	
		})
	})

});