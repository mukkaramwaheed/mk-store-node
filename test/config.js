let chai = require('chai');
let chaiHttp = require('chai-http');
// let chaiLike = require('chai-like');
// let chaiThings = require('chai-things');
var should = chai.should();
var request = require('supertest');
var expect = require('chai').expect;
var server = require('../server');
var userData = require('./seeder/user');
// var chaiAsPromised = require('chai-as-promised');

chai.use(chaiHttp);
var { constants } = require('./constants');
// chai.use(chaiLike);
// chai.use(chaiThings);
// chai.use(require('chai-like'));
// chai.use(require('chai-things'));

module.exports = {
    chai,
    server,
    userData,
    expect,
    constants
}