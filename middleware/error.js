const ErrorResponse = require('../utils/errorResponse');
const { validationResult } = require('express-validator');
const logger = require('../wiston-config');

const   errorHandler = (err, req, res, next) => {
  let error = { ...err };

  error.message = err.message;

  // Log to console for dev
  console.log(err);
  

  // Mongoose bad ObjectId
  if (err.name === 'CastError') {
    const message = `Resource not found`;
    logger.error(error.message);
    error = new ErrorResponse(message, 404);
  }

  if(err.code === 'LIMIT_UNEXPECTED_FILE'){

    const message = 'Maximum file upload limit is ' + req.limit;
    logger.error(error.message);
    error = new ErrorResponse(message, 400);
  }
  // Mongoose duplicate key
  else if (err.code === 11000) {
    const message = 'Record already exist with this name';
    logger.error(error.message);
    error = new ErrorResponse(message, 400);
  }

  // Mongoose validation error
  if (err.name === '' || typeof err.name == 'undefined') {
    // if (err.name === 'ValidationError') {

    return res.status(400).json({
      success: false,
      error: err
    });

    // const message = Object.values(err.errors).map(val => val.message);
    // error = new ErrorResponse(message, 400);
  }

  res.status(error.statusCode || 500).json({
    success: false,
    error: error.message || 'Server Error'
  });
};

module.exports = errorHandler;
