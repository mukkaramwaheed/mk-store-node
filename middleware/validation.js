const { validationResult } = require('express-validator');


module.exports.validation = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next()
    }
    // err.message = 'ValidationError';
    
    const extractedErrors = []
    errors.array({ onlyFirstError: true }).map((err) => extractedErrors.push({ [err.param]: err.msg }))
    next(extractedErrors);
   
  }