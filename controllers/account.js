const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const User = require('../models/User');
const logger = require('../wiston-config');
const { constants } = require('../helpers/contants');
const { deleteImageFromFolder } = require('../helpers/utils');


/**
 * Get user profile
 * @return {Object}
 */


exports.getProfile = asyncHandler(async (req, res, next) => {
  
  const user = await User.findById(req.user._id);
  const profile = user.getProfile();
  
  logger.info("Profile get");
  res.status(200).json({
    success: true,
    data: profile
  });
});


/**
 * Update user profile
 * 
 * 
 * @return {Object}
 */
exports.updateProfile = asyncHandler(async (req, res, next) => {


  //Check image name
  logger.info("image name for user profile " + req.body.avatar);
  if(typeof req.body.avatar !== 'undefined'){
    const oldImage = await User.findById(req.user._id);

    //oldImage.avatar exist then delete the previous image from directory
    if(oldImage.avatar){
  
      deleteImageFromFolder(constants.imageDir,oldImage.avatar)
      logger.info(`Old image deleted from ${constants.imageDir}`);
    }
  }

  const user = await User.findByIdAndUpdate(req.user._id, req.body, {
    new: true,
    runValidators: true
  });

  const profile = user.getProfile();
  logger.info("Update user profile")

  res.status(200).json({
    success: true,
    data: profile
  });
});

/**
 * Update password
 * 
 * 
 * @return {Object}
 */

exports.changePassword = asyncHandler(async (req,res,next) => {

    const user = await User.findById(req.user._id).select('+password');
    
     // Check current password
    if (!(await user.matchPassword(req.body.currentPassword))) {

      logger.error("Current password is incorrect");
      return next(new ErrorResponse('Current Password is incorrect', 401));
    }
    
    // newPassword and currentPassword must be different
    if (req.body.currentPassword === req.body.newPassword) {

      logger.error("New password must be different from the current password");
      return next(new ErrorResponse('New password must be different from the current password', 401));
    }
    
    user.password = req.body.newPassword;
    await user.save();

    logger.info("Password updated");

  
    res.status(200).json({
      success: true
    });
})

/**
 * Delete Image
 * 
 * @return {Object}
 */

exports.deleteImage = asyncHandler(async (req, res, next) => {


  const oldImage = await User.findById(req.user._id);

  //oldImage.avatar exist then delete the previous image from directory
  if(oldImage.avatar){

    deleteImageFromFolder(constants.imageDir,oldImage.avatar)
    logger.info(`Delete image from ${constants.imageDir}`);
    await User.update({},{'$unset':{'avatar':1}});

    res.status(200).json({
      success: true
    });
  }
  else{
    logger.error("Image not found");
    return next(new ErrorResponse('Image not found', 404));
  }


  
});


// @desc      Delete user
// @route     DELETE /api/v1/users/:id
// @access    Private
// exports.deleteUser = asyncHandler(async (req, res, next) => {
//   await User.findByIdAndDelete(req.params.id);

//   res.status(200).json({
//     success: true,
//     data: {}
//   });
// });
